%% binomialMergePools
% 
% Computes the generalized recall, as defined by [Kek�l�inen and J�rvelin, 2002]. 
% Generalized recall works only with graded relevance judgments and it is 
% not possible to ask for a mapping to binary relevance.

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = generalizedRecall(pool, runSet, Name, Value)
%  
% It returns NaN if there are no relevant documents in the pool.
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html 
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html 
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html 
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|MapToRelevanceWeights|* (optional) - a vector of numbers to which the
% relevance degrees in the pool will be mapped. It must be an increasing
% vector with as many elements as the relevance degrees in the pool are.
% The default is |[0, 1, 2, ...]| up to as many relevance degrees are in
% the pool.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is 
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column 
% for each run named |runName|. Each cell of the table contains a scalar
% representing the generalized recall. The |UserData| property of  the table 
% contains a struct  with the  following fields: _|identifier|_ is the 
% identifier of the run; _|name|_  is the name of the computed measure, i.e.
% |generalizedRecall|; _|shortName|_ is a short name of the computed 
% measure, i.e. |gR|; _|pool|_ is the identifier of the pool with respect 
% to which the measure has been computed. 
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.
%
%% References
% 
% Please refer to :
%
% * Kek�l�inen, J. and J�rvelin, K. (2002). Using Graded Relevance Assessments
% in IR Evaluation. _Journal of the American Society for Information Science and Technology (JASIST)_, 
% 53(13):1120-1129.
% 
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Author*: <mailto:elosiouk@math.unipd.it Eleonora Losiouk>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2018b or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [mergedPool] = sqmvMerger(varargin)

    % check that we have at least two pools
    narginchk(2, inf);
        
    mergedPool = varargin{1};
    
    % the possible relevance degrees according to the pool
    relevanceDegrees = categories(varargin{1}{:, 1}{1, 1}.RelevanceDegree);
    
    assessors = length(varargin);
                
    for t = 1:height(mergedPool)
        
        topic = mergedPool{t, 1}{1, 1};
        
        % discard the categorical relevance degree column and substitute
        % with a double one
        topic(:, 2) = [];
        topic{:, 2} = NaN;
        topic.Properties.VariableNames = {'Document', 'RelevanceDegree'};        
        
        p = NaN(height(topic), assessors);
                
        for k = 1:assessors            
            p(:, k) = double(varargin{k}{t, 1}{1, 1}{:, 2} > relevanceDegrees{1});
        end
                        
        topic{:, 2} =  sigmoid(sum(p, 2) / assessors, 15);
        
        mergedPool{t, 1}{1, 1} = topic;
                
    end % for topic
    
end