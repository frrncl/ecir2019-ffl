%% expectedRandomRankBiasedPrecision
%
% Computes rank-biased precision (eRRBP).

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = expectedRandomRankBiasedPrecision(pool, runSet, Name, Value)
%
% Note that rank-biased precision will be NaN when there are no relevant
% documents for a given topic in the pool (this may happen due to the way
% in which relevance degrees are mapped to binary relevance).
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the
% argument name and |Value| is the corresponding value. |Name| must appear
% inside single quotes (' '). You can specify several name and value pair
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|RelevanceProbability|* (mandatory) - a numeric vector of
% probabilities that a user sees as relevant the given relevance degree.
% It must have as many elements as the number of relevance degrees.
% * *|Persistence|* (optional) - a real value providing the persistence
% parameter of eRRBP. The default is 0.8.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|NotAssessed|* (optional) - a string indicating how not assessed
% documents, i.e. those in the run but not in the pool, have to be
% processed: |NotRevelant|, the minimum of the relevance degrees of the
% pool is used as |NotRelevant|; |Condensed|, the not assessed documents
% are  removed from the run. If not specified, the default value  is
% |NotRelevant| to mimic the behaviour of trec_eval.
% * *|MapToBinaryRelevance|* (optional) - a string specifying how relevance
% degrees have to be mapped to binary relevance. The following values can
% be used: _|Hard|_ considers only the maximum degree of relevance in the
% pool as |Relevant| and any degree below it as |NotRelevant|; _|Lenient|_
% considers any degree of relevance in the pool above the minimum one as
% |Relevant| and only the minimum one is considered as |NotRelevant|;
% _|RelevanceDegree|_ considers the relevance degrees in the pool stricly
% above the specified one as |Relevant| and all those less than or equal to
% it as |NotRelevant|. In this latter case, if |RelevanceDegree| does not
% correspond to any of the relevance degrees in the pool, an error is
% raised. If not specified, |Lenient| will be used to map to binary
% relevance.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column
% for each run named |runName|. Each cell of the table contains a scalar
% representing the average precision. The |UserData| property of  the table
% contains a struct  with the  following fields: _|identifier|_ is the
% identifier of the run; _|name|_  is the name of the computed measure, i.e.
% |expectedRandomRankBiasedPrecision|; _|shortName|_ is a short name of the computed
% measure, i.e. |eRRBP|; _|pool|_ is the identifier of the pool with respect
% to which the measure has been computed. Note that when the condensed
% measure is requested, as in (Sakai, SIGIR 2007), then the name and short
% name are, respectively, |condensedexpectedRandomRankBiasedPrecision| and |coneRRBP|.
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.

%% Example of use
%
%
%
%% References
%
%
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [measuredRunSet, inputParams] = expectedRandomDiscountedCumulatedGain(pool, runSet, varargin)

    % check that we have the correct number of input arguments.
    narginchk(2, inf);

    % parse the variable inputs
    pnames = {'ShortNameSuffix', 'FixNumberRetrievedDocuments' 'Verbose'};
    dflts =  {[]                 1000                           false};

    if verLessThan('matlab', '9.2.0')
        [shortNameSuffix, fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [shortNameSuffix, fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end

    % actual parameters to be passed to binomialAssess.m, at least 6
    assessInput = cell(1, 2);

    % padding is not needed
    assessInput{1, 1} = 'FixNumberRetrievedDocuments';
    assessInput{1, 2} = fixNumberRetrievedDocuments;

    if supplied.ShortNameSuffix
        if iscell(shortNameSuffix)
            % check that nameSuffix is a cell array of strings with one element
            assert(iscellstr(shortNameSuffix) && numel(shortNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected NameSuffix to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        shortNameSuffix = char(strtrim(shortNameSuffix));
        shortNameSuffix = shortNameSuffix(:).';

        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(shortNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'NameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                shortNameSuffix);
        end

        % if it starts with an underscore, remove it since il will be
        % appended afterwards
        if strcmp(shortNameSuffix(1), '_')
            shortNameSuffix = shortNameSuffix(2:end);
        end
    end

    if supplied.Verbose
        % check that verbose is a non-empty scalar logical value
        validateattributes(verbose, {'logical'}, {'nonempty','scalar'}, '', 'Verbose');
    end

    if verbose
        fprintf('\n\n----------\n');

        fprintf('Computing discounted cumulated gain for run set %s with respect to pool %s: %d run(s) and %d topic(s) to be processed.\n\n', ...
            runSet.Properties.UserData.identifier, pool.Properties.UserData.identifier, width(runSet), height(runSet));
    end

    [assessedRunSet, inputParams] = binomialAssess(pool, runSet, 'Verbose', verbose, assessInput{:});

    % the topic currently under processing
    ct = 1;

    % the run currently under processing
    cr = 1;

    % compute the measure topic-by-topic
    measuredRunSet = rowfun(@processTopic, assessedRunSet, 'OutputVariableNames', runSet.Properties.VariableNames, 'OutputFormat', 'table', 'ExtractCellContents', true, 'SeparateInputs', false);
    measuredRunSet.Properties.UserData.identifier = assessedRunSet.Properties.UserData.identifier;
    measuredRunSet.Properties.UserData.pool = pool.Properties.UserData.identifier;

    measuredRunSet.Properties.UserData.name = 'expectedRandomDiscountedCumulatedGain';
    measuredRunSet.Properties.UserData.shortName = 'eRDCG';

    if ~isempty(shortNameSuffix)
        measuredRunSet.Properties.UserData.shortName = [measuredRunSet.Properties.UserData.shortName ...
            '_' shortNameSuffix];
    end

    if verbose
        fprintf('Computation of expected random discounted cumulated gain completed.\n');
    end

%%

% compute the measure for a given topic over all the runs
    function [varargout] = processTopic(topic)
        
        if(verbose)
            fprintf('Processing topic %s (%d out of %d)\n', pool.Properties.RowNames{ct}, ct, inputParams.topics);
            fprintf('  - run(s): ');
        end;
        
        % reset the index of the run under processing for each topic
        cr = 1;
        
        % compute the measure only on those column which contain the
        % actual runs
        varargout = cellfun(@processRun, topic);
        
        % increment the index of the current topic under processing
        ct = ct + 1;
        
        if(verbose)
            fprintf('\n');
        end;
        
        %%
        
        % compute the measure for a given topic of a given run
        function [measure] = processRun(runTopic)
            
            if(verbose)
                fprintf('%s ', runSet.Properties.VariableNames{cr});
            end
            
            vect = runTopic{:, 'Assessment'};
            
            % find the positions of the relevant documents
            relPos = find(vect);
            
            % if there are no relevant documents, skip useless computations
            % and return 0
            if ~any(relPos)
                 measure = {0};
                 
                % increment the index of the current run under processing
                cr = cr + 1;
                 
                return;
            end
                      
            measure = sum(vect(relPos) ./ max(1, log10(relPos)));
            
            % properly wrap the results into a cell in order to fit it into
            % a value for a table
            measure = {measure};
            
            % increment the index of the current run under processing
            cr = cr + 1;
        end
    end

end



