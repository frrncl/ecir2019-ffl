%% compute_rndpool_gap
% 
% Compute a gap on random pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_rndpool_gap(trackID, tag)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|gap|* - the gap to be computed.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rndpool_gap(trackID, tag, gap, granularity, aggregation, mid)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

    if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';

    % check that gap is a non-empty string
    validateattributes(gap,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'gap');

    if iscell(gap)
        % check that gap is a cell array of strings with one element
        assert(iscellstr(gap) && numel(gap) == 1, ...
            'MATTERS:IllegalArgument', 'Expected gap to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    gap = char(strtrim(gap));
    gap = gap(:).';
    
    % check that granularity is a non-empty string
    validateattributes(granularity,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'granularity');

    if iscell(granularity)
        % check that granularity is a cell array of strings with one element
        assert(iscellstr(granularity) && numel(granularity) == 1, ...
            'MATTERS:IllegalArgument', 'Expected granularity to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    granularity = char(strtrim(granularity));
    granularity = granularity(:).';
    
    % check that aggregation is a non-empty string
    validateattributes(aggregation,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'aggregation');

    if iscell(aggregation)
        % check that aggregation is a cell array of strings with one element
        assert(iscellstr(aggregation) && numel(aggregation) == 1, ...
            'MATTERS:IllegalArgument', 'Expected aggregation to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    aggregation = char(strtrim(aggregation));
    aggregation = aggregation(:).';
    
    
    % check that mid is a non-empty string
    validateattributes(mid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');

    if iscell(mid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(mid) && numel(mid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    mid = char(strtrim(mid));
    mid = mid(:).';    
     
    
    % setup common parameters
    common_parameters;          
    
    
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Computing %s %s %s gap to %s on collection %s (%s) ########\n\n', granularity, aggregation, gap, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - granularity %s\n', granularity);
    fprintf('  - aggregation %s\n', aggregation);
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end;
  
    
    % load the base data
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'poolLabels', 'P');

    
    fprintf('+ Loading assessor measures\n');
    
    % the assessor measures 
    assessorMeasures = cell(1, P, originalTrackNumber);
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
                
            
            % for each assessor
            for p = 1:P
                
                measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                                
                serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID), measureID);
                
                eval(sprintf('assessorMeasures{1, p, r} = %1$s{:, :};', measureID));
                
                clear(measureID);
                
            end; % for assessor
        
    end; % for runset
    
    clear r m p measureID
    
    
    fprintf('\n+ Loading %s measures\n', tag);
    
    % the random measures
    randomMeasures = cell(1, EXPERIMENT.analysis.rndPoolsSamples, originalTrackNumber);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        fprintf('    ');
        
        % for each random pool
        for p = 1:EXPERIMENT.analysis.rndPoolsSamples
            
            sampledPool = EXPERIMENT.pattern.identifier.sampledRndPool(p);
            
                
                measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, sampledPool, shortTrackID, originalTrackShortID{r});
                
                serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.(tag).relativePath, tag, measureID), measureID);
                
                eval(sprintf('randomMeasures{1, p, r} = %1$s{:, :};', measureID));
                
                clear(measureID);
                
            
            
            fprintf('.');
            
        end; % for random pool
        
        fprintf('\n');
        
    end; % for runset
    
    clear r m p measureID
    
    
    fprintf('\n+ Computing %s gap with %s measures\n', gap, tag);
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % number of topics
        T = size(randomMeasures{1, 1, r}, 1);
        
        % number of runs
        R = size(randomMeasures{1, 1, r}, 2);
        
            
            fprintf('    * %s\n', mid);
            fprintf('      ');
                        
            % average random measures before further processing
            if strcmpi(aggregation, 'msr')
                
                % Each plane is a TxR measure, there are rndPoolSamples
                % planes
                random = NaN(T, R, EXPERIMENT.analysis.rndPoolsSamples);
                
                % copy from the cell matrix in the corresponding plane
                for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                    random(:, :, rp) = randomMeasures{1, rp, r};
                end;
                
                % average over all the planes
                random = mean(random, 3);
                
                % for each assessor pool
                for ap = 1:P
                    
                    % the assessor measures
                    assessor = assessorMeasures{1, ap, r};
                    
                    gapID = EXPERIMENT.pattern.identifier.pm(gap, mid, tag, poolLabels{ap}, shortTrackID, originalTrackShortID{r});
                    
                    % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                        evalf(EXPERIMENT.command.rndPools.gap.(granularity).(gap), ...
                            {'assessor', 'random'}, ...
                            {gapID});
                        
                    else % different gap for each topic
                        
                        % one gap for each topic
                        eval(sprintf('%1$s = NaN(1, T);', gapID));
                        
                        for t = 1:T
                            
                            assessorTopic = assessor(t, :);
                            randomTopic = random(t, :);
                            
                            evalf(EXPERIMENT.command.rndPools.gap.(granularity).(gap), ...
                                {'assessorTopic', 'randomTopic'}, ...
                                {'tmp'});
                            
                            eval(sprintf('%1$s(t) = tmp;', gapID));
                            
                        end;
                        
                        clear tmp assessorTopic randomTopic;
                        
                    end;
                                        
                    sersave(EXPERIMENT.pattern.file.pre(granularity, aggregation, gap, trackID, tag, gapID), gapID(:));
                    fprintf('.');
                     
                    clear gapID assessor;
                    
                end; % for each assessor
                
                clear random;
                                
            else
                
                
                % for each assessor pool
                for ap = 1:P
                    
                    % the assessor measures
                    assessor = assessorMeasures{1, ap, r};
                    
                    gapID = EXPERIMENT.pattern.identifier.pm(gap, mid, tag, poolLabels{ap}, shortTrackID, originalTrackShortID{r});
                                                            
                    % single gap for all the topics
                    if strcmpi(granularity, 'sgl')
                        
                        eval(sprintf('%1$s = NaN(1, EXPERIMENT.analysis.rndPoolsSamples);', gapID));
                        
                        % for each random pool
                        for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                            
                            % the random pool measures
                            random = randomMeasures{1, rp, r};
                            
                            evalf(EXPERIMENT.command.rndPools.gap.(granularity).(gap), ...
                                {'assessor', 'random'}, ...
                                {'tmp'});
                            
                            % the assessor weight is proportional to its AP correlation
                            eval(sprintf('%1$s(rp) = tmp;', gapID));
                            
                            clear random tmp;
                            
                        end; % for each random pool
                                                
                    else % different gap for each topic
                        
                         
                        eval(sprintf('%1$s = NaN(T, EXPERIMENT.analysis.rndPoolsSamples);', gapID));
                        
                        % for each random pool
                        for rp = 1:EXPERIMENT.analysis.rndPoolsSamples
                            
                            % the random pool measures
                            random = randomMeasures{1, rp, r};
                            
                            for t = 1:T
                                
                                assessorTopic = assessor(t, :);
                                randomTopic = random(t, :);
                                
                                evalf(EXPERIMENT.command.rndPools.gap.(granularity).(gap), ...
                                    {'assessorTopic', 'randomTopic'}, ...
                                    {'tmp'});
                                
                                eval(sprintf('%1$s(t, rp) = tmp;', gapID));
                                
                            end;
                            
                            clear tmp random assessorTopic randomTopic;
                                                        
                        end; % for each random pool
                        
                    end;
                    
                    
                    sersave(EXPERIMENT.pattern.file.pre(granularity, aggregation, gap, trackID, tag, gapID), gapID(:));                    
                    fprintf('.');
                    
                    clear assessor gapID;
                    
                end; % for each assessor
                
                
                
                
                
                
            end;
            
            
            
            fprintf('\n');
            
    end; % for runset
    
    fprintf('\n\n######## Total elapsed time for computing %s %s %s gap to %s on collection %s (%s): %s ########\n\n', ...
        granularity, aggregation, gap, tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;
    
end
