%% compute_base_measures
% 
% Computes measures for the base pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_base_measures(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_bizbase_measures(trackID, mid)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that mid is a non-empty string
    validateattributes(mid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');

     if iscell(mid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(mid) && numel(mid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    mid = char(strtrim(mid));
    mid = mid(:).';    
    
    % setup common parameters
    common_parameters;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing binomialized %s measures on collection %s (%s) ########\n\n', EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    start = tic;
    fprintf('  - loading the dataset\n');

    %serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID));
    serload('/Users/ferro/Documents/pubblicazioni/2019/ECIR2019/FFL/experiment/dataset/TREC_21_2012_Crowd/bizbase/dataset_T21.mat')

    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % Adding the gold pool label and identifier to run everything in one cycle
    poolLabels = [{EXPERIMENT.label.goldPool} poolLabels];
    poolIdentifiers = [replace({EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID)}, '_base', '_bizbase') poolIdentifiers];

    % for each pool
    for p = 1:length(poolLabels)

        startPool = tic;

        fprintf('\n+ Pool: %s\n', poolIdentifiers{p});   
        
        % for each runset
        for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
            
            fprintf('  - original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
            
            start = tic;
            
            fprintf('    * computing %s\n', mid);
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, ['biz' EXPERIMENT.tag.base.id], poolLabels{p}, ...
                EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
            
            shortNameSuffix = poolLabels{p};
            evalf(EXPERIMENT.command.measure.(mid), ...
                {poolIdentifiers{p}, EXPERIMENT.pattern.identifier.runSet(EXPERIMENT.(trackID).shortID, EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r}), 'shortNameSuffix'}, ...
                {measureID});
            
            sersave(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, ['biz' EXPERIMENT.tag.base.id], measureID), measureID(:));
            
            % free space
            clear(measureID);
            
            fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            
            
            
            % free the assess cache
            clear assess
            
        end; % for runset
        

        fprintf('  - elapsed time for pool %s: %s\n', poolIdentifiers{p}, elapsedToHourMinutesSeconds(toc(startPool)));

    end; % for pools


    fprintf('\n\n######## Total elapsed time for computing %s measures on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
