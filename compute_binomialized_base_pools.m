%% compute_base_measures
% 
% Computes measures for the base pools and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_base_measures(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_binomialized_base_pools(trackID, p_notrel, p_rel)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
   
    % setup common parameters
    common_parameters;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s binomialized base pools on collection %s (%s) ########\n\n', EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    start = tic;
    fprintf('  - loading the dataset\n');

    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID));

    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % Adding the gold pool label and identifier to run everything in one cycle
    poolLabels = [{EXPERIMENT.label.goldPool} poolLabels];
    poolIdentifiers = [{ EXPERIMENT.pattern.identifier.goldPool(EXPERIMENT.(trackID).shortID)} poolIdentifiers];

    bizPoolIdentifiers = replace(poolIdentifiers, '_base', '_bizbase');
    
    % for each pool
    for p = 1:length(poolLabels)

        startPool = tic;
        
        fprintf('\n+ Pool: %s\n', poolIdentifiers{p});  
        
        
        eval(sprintf('pool = %s;', poolIdentifiers{p}));
        
        bizPool = pool;
        
        for t = 1:height(pool)
            
            topic = pool{t, 1}{1, 1};
            
            notRel = topic{:, 2} == 'NotRelevant';
            
            topic(:, 2) = [];
            topic{:, 2} = p_rel;
            topic{notRel, 2} = p_notrel;
            topic.Properties.VariableNames = {'Document', 'RelevanceDegree'};
            
            bizPool{t, 1}{1, 1} = topic;
            
        end;
        
        bizPool.Properties.UserData.identifier = bizPoolIdentifiers{p};
        bizPool.Properties.UserData.fileName = bizPoolIdentifiers{p};
        
        eval(sprintf('%s = bizPool;', bizPoolIdentifiers{p}));

        clear pool bizPool;
        clear(poolIdentifiers{p});
        
    end
    
    runsetIDs = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        
        runsetIDs{r} = EXPERIMENT.pattern.identifier.runSet(EXPERIMENT.(trackID).shortID, EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
        
    end; % for runset
    
    
    poolIdentifiers = bizPoolIdentifiers(2:end);
    poolLabels = poolLabels(2:end);
    
    variables = [bizPoolIdentifiers 'poolIdentifiers', 'poolLabels', 'P', 'T', runsetIDs];
    
    sersave('/Users/ferro/Documents/pubblicazioni/2019/ECIR2019/FFL/experiment/dataset/TREC_21_2012_Crowd/bizbase/dataset_T21.mat', variables{:})
    


    fprintf('\n\n######## Total elapsed time for computing %s measures on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
