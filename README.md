Matlab source code for running the experiments reported in the paper:

* Ferrante, M., Ferro, N., and Losiouk, E. (2019). Stochastic Relevance for Crowdsourcing. In Fuhr, N., Mayr, P., Azzopardi, L., Stein, B., Hauff, C., and Hiemstra, D., editors, _Advances in Information Retrieval. Proc. 41st European Conference on IR Research (ECIR 2019)_. Lecture Notes in Computer Science (LNCS), Springer, Heidelberg, Germany.
