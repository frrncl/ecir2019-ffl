%% compute_binmv_pools
% 
% Compute the majority vote pools for and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_binmv_pools(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = compute_s10qbinmv_pools(trackID)

     % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s pools on collection %s (%s) ########\n\n', EXPERIMENT.tag.s20qbinmv.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

    start = tic;

    fprintf('  - Loading data\n');
    
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));

    % load the pools identifiers
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'P', 'poolIdentifiers');

    % load the pools
    for p = 1:P
        serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), poolIdentifiers{p});
    end;
    
    fprintf('    * %d pool(s) loaded\n', P);

    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % for each k-uple
    for k = 1:EXPERIMENT.kuples.number

        start = tic;

        fprintf('  - k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));

        evalf(EXPERIMENT.command.util.assignVars, ...
            {kuplesIdentifiers{k}}, ...
            {'currentKuples'});

        for kk = 1:size(currentKuples, 1)

            poolID = EXPERIMENT.pattern.identifier.pool(EXPERIMENT.tag.s20qbinmv.id, ...
                   EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), ...
                   EXPERIMENT.(trackID).shortID);
                                    
            evalf(EXPERIMENT.command.s20qbinmvPools.compute, ...
                [{'poolID'}, poolIdentifiers(currentKuples(kk, :))], ...
                {poolID});
            
            sersave(EXPERIMENT.pattern.file.pool(trackID, EXPERIMENT.tag.s20qbinmv.id, poolID), poolID(:));

            clear(poolID);
        end;

        fprintf('     * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    end;


    fprintf('\n\n######## Total elapsed time for computing %s pools on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.tag.s20qbinmv.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
        
    diary off;
end
