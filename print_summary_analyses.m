%% print_summary_analyses
% 
% Print the summary analyses and saves them tex file.
%
%% Synopsis
%
%   [] = print_summary_analyses(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = print_summary_analyses(trackID, goldmid, mid)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that goldmid is a non-empty string
    validateattributes(goldmid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'goldmid');

     if iscell(goldmid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(goldmid) && numel(goldmid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected goldmid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    goldmid = char(strtrim(goldmid));
    goldmid = goldmid(:).';    
    
    % check that mid is a non-empty string
    validateattributes(mid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');

     if iscell(mid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(mid) && numel(mid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    mid = char(strtrim(mid));
    mid = mid(:).';        
    
    % setup common parameters
    common_parameters;
            
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Printing summary analyses on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    %selectedKuplesIdx = [1:9 14 19 24 29];
    selectedKuplesIdx = [2 3 4 5 10 20 30] - 1;
    selectedKuplesNumber = length(selectedKuplesIdx);
    
    % the labels of the experiments
    labels = [EXPERIMENT.taxonomy.baseline.list list_Systems()] ;
    tags = length(labels);
    
    
    fprintf('+ Loading analyses\n');
    
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        apcBase.(originalTrackShortID{r}).mean = NaN(1, EXPERIMENT.measure.number);
        rmseBase.(originalTrackShortID{r}).mean = NaN(1, EXPERIMENT.measure.number);
        
        
        fprintf('    * base measure:\n');
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('      # %s \n', EXPERIMENT.measure.name{m});
            
            measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID), ...
                {apcID, 'apcTmp';
                rmseID, 'rmseTmp'});
            
            
            apcBase.(originalTrackShortID{r}).mean(m) = apcTmp.mean;
            rmseBase.(originalTrackShortID{r}).mean(m) = rmseTmp.mean;
            
            clear apcTmp rmseTmp
        end;
                        
        apc.(originalTrackShortID{r}).mean = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        rmse.(originalTrackShortID{r}).mean = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            for e = 1:tags
                
                 fprintf('      # experiment: %s \n', labels{e});
                
                for k = 1:selectedKuplesNumber
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, labels{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, labels{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, labels{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    
                    try
                        serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(labels{e}).relativePath, labels{e}, measureID), ...
                            {apcID, 'apcTmp';
                            rmseID, 'rmseTmp'});
                        
                        apc.(originalTrackShortID{r}).mean(e, k, m) = apcTmp.mean;
                        rmse.(originalTrackShortID{r}).mean(e, k, m) = rmseTmp.mean;
                        
                        clear apcTmp rmseTmp
                                                
                    catch exception
                        fprintf('        File not found: %s\n', EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(labels{e}).relativePath, labels{e}, measureID));
                        
                    end;
                    
                end; % kuples
                
            end; % tags
            
        end % measures
        
        
    end; % for each runset
    
    
    fprintf('+ Printing the report\n');    

    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report('summary', trackID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{colortbl} \n');
    fprintf(fid, '\\usepackage{multirow} \n');
    fprintf(fid, '\\usepackage{lscape} \n');
    fprintf(fid, '\\usepackage{pdflscape} \n');
    fprintf(fid, '\\usepackage{longtable} \n');
    fprintf(fid, '\\usepackage[x11names]{xcolor}\n\n');

    fprintf(fid, '\\begin{document}\n\n');

    fprintf(fid, '\\title{Summary Report on %s}\n\n', EXPERIMENT.(trackID).name);

    fprintf(fid, '\\maketitle\n\n');
    
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - %s \n', EXPERIMENT.measure.name{m});
        
        %############################################ AP Correlation Table
        fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\centering \n');
        fprintf(fid, '\\footnotesize \n');
        
        fprintf(fid, '\\begin{longtable}{|ll|*{%d}{|c}|c|}\n', selectedKuplesNumber - 1);
        fprintf(fid, '\\caption{AP correlation for %s on collection %s using runs from %s and %s.\\label{tab:apc_%s}} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2}, ...
            EXPERIMENT.measure.id{m});
                
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\multicolumn{%d}{|c|}{\\textbf{%s}} \\\\ \n', selectedKuplesNumber - 1 + 3, EXPERIMENT.measure.fullName{m});
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\multicolumn{2}{|c||}{\\multirow{3}{*}{\\textbf{Method}}}  & \\multicolumn{%d}{c|}{\\textbf{AP Correlation (Root Mean Square Error)}} \\\\ \n', ...
             selectedKuplesNumber);
         
         fprintf(fid, '\\multicolumn{2}{|c||}{}                                  & \\multicolumn{%d}{c|}{\\textbf{Runs \\texttt{%s}: %0.4f (%0.4f) -- Runs \\texttt{%s}: %0.4f (%0.4f)}} \\\\ \n', ...
             selectedKuplesNumber, ...
             originalTrackShortID{1}, apcBase.(originalTrackShortID{1}).mean(m), rmseBase.(originalTrackShortID{1}).mean(m), ...
             originalTrackShortID{2}, apcBase.(originalTrackShortID{2}).mean(m), rmseBase.(originalTrackShortID{2}).mean(m));
        
        fprintf(fid, '\\cline{3-%d} \n', selectedKuplesNumber - 1 + 3);
        fprintf(fid, '\\multicolumn{2}{|c||}{} ');
        
        for k = 1:selectedKuplesNumber            
            fprintf(fid, ' & \\textbf{k=%d} ', selectedKuplesIdx(k) + 1);            
        end;
        fprintf(fid, ' \\\\* \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endfirsthead \n');
        
        fprintf(fid, '\\caption{AP correlation for %s on collection %s using runs from %s and %s.} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2});
        
        fprintf(fid, '\\cline{3-%d} \n', selectedKuplesNumber - 1 + 3);
        fprintf(fid, ' & ');
        
        for k = 1:selectedKuplesNumber            
            fprintf(fid, ' & \\textbf{k=%d} ', selectedKuplesIdx(k) + 1);            
        end;
        fprintf(fid, ' \\\\* \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endhead \n');
        
        for e = 1:tags
            
            fprintf(fid, '                                & \\hspace*{-1.5em}\\texttt{%s} ', ...
                originalTrackShortID{1});
            
            for k = 1:selectedKuplesNumber
                 fprintf(fid, ' & %0.4f (%0.4f) ', ...
                     apc.(originalTrackShortID{1}).mean(e, k, m), ...
                     rmse.(originalTrackShortID{1}).mean(e, k, m));
            end;
                        
            fprintf(fid, ' \\\\ \n');
            
            
            fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & \\hspace*{-1.5em}\\texttt{%s}', ...
                EXPERIMENT.tag.(labels{e}).latex.label, originalTrackShortID{2});
            
            
            for k = 1:selectedKuplesNumber
                 fprintf(fid, ' & %0.4f (%0.4f) ', ...
                     apc.(originalTrackShortID{2}).mean(e, k, m), ...
                     rmse.(originalTrackShortID{2}).mean(e, k, m));
            end;
                        
            fprintf(fid, ' \\\\ \n');
            
            
            fprintf(fid, '\\hline \n');
        end; % for each experiment
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{longtable} \n');
        
        fprintf(fid, '\\end{landscape} \n\n\n');

        
    end;
            
    fprintf(fid, '\\end{document}');
    
    fclose(fid);
    

    fprintf('\n\n######## Total elapsed time for printing summary analyses on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
