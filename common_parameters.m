%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


diary off;


%% General configuration



% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
elseif (strcmpi(computer, 'PCWIN64'))
    addpath('G:\My Drive\Padova\Ferro_Ferrante\core\core\analysis\')
    addpath('G:\My Drive\Padova\Ferro_Ferrante\core\core\io\')
    addpath('G:\My Drive\Padova\Ferro_Ferrante\core\core\measure\')
    addpath('G:\My Drive\Padova\Ferro_Ferrante\core\core\plot\')
    addpath('G:\My Drive\Padova\Ferro_Ferrante\core\core\util\')
end

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/ECIR2019-FFL/experiment/';
elseif(strcmpi(computer, 'PCWIN64'))
    EXPERIMENT.path.base = 'C:\Users\eleon\Dropbox\IRJ2018\experiment\';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2019/ECIR2019/FFL/experiment/';  
end


% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);


% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'ECIR 2019 FFL';

% Label to be used for the gold standard pool
EXPERIMENT.label.goldPool = 'GOLD';


%% Overall Experiment Taxonomy

% Command to create the tag of an experiment from its components
EXPERIMENT.taxonomy.tag = @(family, learning, feature, granularity, aggregation, gap, weight) ...
    sprintf('%1$s_%2$s_%3$s_%4$s_%5$s_%6$s_%7$s', ...
        family, learning, feature, granularity, aggregation, gap, weight);

    
% The baseline approaches to confront with
EXPERIMENT.taxonomy.baseline.id = 'baseline';
EXPERIMENT.taxonomy.baseline.description = 'Baseline approaches';
EXPERIMENT.taxonomy.baseline.list = {'mv', 'emmv', 'aw_uni', 'aw_sgl_tau_msd', 'aw_sgl_rmse_med'};    
EXPERIMENT.taxonomy.baseline.number = length(EXPERIMENT.taxonomy.baseline.list);


% EXPERIMENT.taxonomy.approaches = EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * ...
%         EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number + ...
%         EXPERIMENT.taxonomy.baseline.number;
%     
% EXPERIMENT.taxonomy.colors = winter(EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number);
% EXPERIMENT.taxonomy.colors = [EXPERIMENT.taxonomy.colors; rgb('Red'); rgb('Brown'); rgb('DarkOrange'); rgb('Orange')];



%% Configuration for tracks

EXPERIMENT.track.list = {'TREC_21_2012_Crowd'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);


EXPERIMENT.TREC_21_2012_Crowd.id = 'TREC_21_2012_Crowd';
EXPERIMENT.TREC_21_2012_Crowd.shortID = 'T21';
EXPERIMENT.TREC_21_2012_Crowd.name =  'TREC 21, 2012, Crowdsourcing';

EXPERIMENT.TREC_21_2012_Crowd.collection.path.base = '/Users/ferro/Documents/experimental-collections/TREC/TREC_21_2012_Crowd/';
EXPERIMENT.TREC_21_2012_Crowd.collection.path.crowd = sprintf('%1$s%2$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'crowd', filesep);
EXPERIMENT.TREC_21_2012_Crowd.collection.path.runSet = @(originalTrackID) sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'runs', filesep, originalTrackID);

EXPERIMENT.TREC_21_2012_Crowd.collection.file.goldPool = sprintf('%1$s%2$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'trat-adjudicated-qrels.txt');

EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id = {'TREC_08_1999_AdHoc', 'TREC_13_2004_Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.shortID = {'T08', 'T13'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.name = {'TREC 08, 1999, AdHoc', 'TREC 13, 2004, Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.number = length(EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id);


%% Configuration for measures

% the identifier of the measures under experimentation
EXPERIMENT.measure.id = {'ap', 'rbp', 'dcg', 'erap', 'errbp', 'erdgc'};

% the number of measures under experimentation
EXPERIMENT.measure.number = length(EXPERIMENT.measure.id);

% the names of the measures under experimentation
EXPERIMENT.measure.name = {'AP', 'RBP', 'DCG', 'ERAP' 'ERRBP', 'ERDCG'};

% the full names of the measures under experimentation
EXPERIMENT.measure.fullName = {'Average Precision (AP)', 'Rank-biased Precision (RBP)', ...
    'Discounted Cumulated Gain (DCG)', ...
    'Expected Random Average Precision (ERAP)', ...
    'Expected Random Rank-biased Precision (ERRBP)', ...
    'Expected Random Discounted Cumulated Gain (ERDCG)'};


%% Configuration for the kuples to be used

% Number of pools/assessors to be merged
EXPERIMENT.kuples.sizes = 2:30;

EXPERIMENT.kuples.labelNames = strtrim(cellstr(num2str(EXPERIMENT.kuples.sizes(:), 'k = %d')));

% The total number of merges to performa
EXPERIMENT.kuples.number = length(EXPERIMENT.kuples.sizes);

% Maximum number of k-uples to be sampled
EXPERIMENT.kuples.samples = 10;


%% Configuration for analyses

% The confidence degree for computing the confidence interval
EXPERIMENT.analysis.ciAlpha = 0.05;

% The number of samples for ties in AP correlation
EXPERIMENT.analysis.apcorrTiesSamples = 100;

% The bandwidth for KDE
EXPERIMENT.analysis.kdeBandwidth = 0.015;

% The bins for kernel density estimation (exceed the range [0 1] to
% compensate for the bandwidth of KSD
EXPERIMENT.analysis.kdeBins = -0.1:0.01:1.1;

% The number of random pools to be generated
EXPERIMENT.analysis.rndPoolsSamples = 10;


%% Configuration for the Expectation-Maximization algorithm

% Maximum number of iterations for expectation maximization algorithm
EXPERIMENT.em.maxIterations = 1000;

% Tolerance for expectation maximization algorithm
EXPERIMENT.em.tolerance = 1e-3;


%% Data for the different experiments

% Baseline data
EXPERIMENT.tag.base.id = 'base';
EXPERIMENT.tag.base.description = 'Baseline data';
EXPERIMENT.tag.base.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);

% Majority vote pools
EXPERIMENT.tag.mv.id = 'mv';
EXPERIMENT.tag.mv.description = 'Majority vote pools';
EXPERIMENT.tag.mv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.mv.latex.label = '\texttt{mv}';
EXPERIMENT.tag.mv.matlab.label = 'mv';

% Binomialized Majority vote pools
EXPERIMENT.tag.bizmv.id = 'bizmv';
EXPERIMENT.tag.bizmv.description = 'Binomialized Majority vote pools';
EXPERIMENT.tag.bizmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.bizmv.latex.label = '\texttt{bizmv}';
EXPERIMENT.tag.bizmv.matlab.label = 'bizmv';


% Expectation-Maximization pools seeded by majority vote
EXPERIMENT.tag.emmv.id = 'emmv';
EXPERIMENT.tag.emmv.description = 'Expectation-Maximization pools seeded by majority vote';
EXPERIMENT.tag.emmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emmv.command = @expectationMaximizationPoolMv;
EXPERIMENT.tag.emmv.latex.label = '\texttt{emmv}';
EXPERIMENT.tag.emmv.matlab.label = 'emmv';


% Binomialized Expectation-Maximization pools seeded by majority vote
EXPERIMENT.tag.bizemmv.id = 'bizemmv';
EXPERIMENT.tag.bizemmv.description = 'Binomialized Expectation-Maximization pools seeded by majority vote';
EXPERIMENT.tag.bizemmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.bizemmv.command = @expectationMaximizationPoolMv;
EXPERIMENT.tag.bizemmv.latex.label = '\texttt{bizemmv}';
EXPERIMENT.tag.bizemmv.matlab.label = 'bizemmv';


% Random uniform pools with assessors judging 5% of the documents as relevant
EXPERIMENT.tag.rnd005.id = 'rnd005';
EXPERIMENT.tag.rnd005.description = 'Random uniform pools with assessors judging 5% of the documents as relevant';
EXPERIMENT.tag.rnd005.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd005.ratio = [0.95 0.05];

% Random uniform pools with assessors judging 50% of the documents as relevant
EXPERIMENT.tag.rnd050.id = 'rnd050';
EXPERIMENT.tag.rnd050.description = 'Random uniform pools with assessors judging 50% of the documents as relevant';
EXPERIMENT.tag.rnd050.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd050.ratio = [0.50 0.50];

% Random uniform pools with assessors judging 95% of the documents as relevant
EXPERIMENT.tag.rnd095.id = 'rnd095';
EXPERIMENT.tag.rnd095.description = 'Random uniform pools with assessors judging 95% of the documents as relevant';
EXPERIMENT.tag.rnd095.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd095.ratio = [0.05 0.95];

% Baseline AWARE: uniform weights for all assessors
EXPERIMENT.tag.aw_uni.id = 'aw_uni';
EXPERIMENT.tag.aw_uni.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.aw_uni.score.command = @aw_uni;
EXPERIMENT.tag.aw_uni.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_uni.latex.label = '\texttt{uni}';
EXPERIMENT.tag.aw_uni.matlab.label = 'uni';


% Majority vote pools
EXPERIMENT.tag.binmv.id = 'binmv';
EXPERIMENT.tag.binmv.description = 'Binomial Majority Vote pools';
EXPERIMENT.tag.binmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.binmv.latex.label = '\texttt{binmv}';
EXPERIMENT.tag.binmv.matlab.label = 'binmv';

% Quantized Majority vote pools
EXPERIMENT.tag.qbinmv.id = 'qbinmv';
EXPERIMENT.tag.qbinmv.description = 'Quantized Binomial Majority Vote pools';
EXPERIMENT.tag.qbinmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.qbinmv.latex.label = '\texttt{qbinmv}';
EXPERIMENT.tag.qbinmv.matlab.label = 'qbinmv';

% Quantized Majority vote pools
EXPERIMENT.tag.s10qbinmv.id = 's10qbinmv';
EXPERIMENT.tag.s10qbinmv.description = 'Sigmoid Quantized Binomial Majority Vote pools with k = 10';
EXPERIMENT.tag.s10qbinmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.s10qbinmv.latex.label = '\texttt{s10qbinmv}';
EXPERIMENT.tag.s10qbinmv.matlab.label = 's10qbinmv';

% Quantized Majority vote pools
EXPERIMENT.tag.sqbinmv.id = 'sqbinmv';
EXPERIMENT.tag.sqbinmv.description = 'Sigmoid Quantized Binomial Majority Vote pools with k = 15';
EXPERIMENT.tag.sqbinmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.sqbinmv.latex.label = '\texttt{sqbinmv}';
EXPERIMENT.tag.sqbinmv.matlab.label = 'sqbinmv';

% Quantized Majority vote pools
EXPERIMENT.tag.s20qbinmv.id = 's20qbinmv';
EXPERIMENT.tag.s20qbinmv.description = 'Sigmoid Quantized Binomial Majority Vote pools with k = 20';
EXPERIMENT.tag.s20qbinmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.s20qbinmv.latex.label = '\texttt{s20qbinmv}';
EXPERIMENT.tag.s20qbinmv.matlab.label = 's20qbinmv';



%>>>>>>>>>> HERE BECAUSE OF MATLAB PARSING
% Commands to be executed to map the different gaps in the range [0, 1] so
% that 0 means different from random assessor and 1 means equal to random
% assessor

% Frobenius norm is in [0, sqrt(T*R)], where 0 means equal to random
% assessor. Divide it by its max and reverse it so that 0 means different
% from random assessor
EXPERIMENT.command.normalize.fro = @(x, T, R) 1 - (x./sqrt(T*R));

% RMSE is in [0, 1] where 0 means equal to random assessor. Reverse it so 
% that 0 means different from random assessor
EXPERIMENT.command.normalize.rmse = @(x, T, R) 1 - x;

% AP correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.apc = @(x, T, R) abs(x);

% Kendall's tau correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.tau = @(x, T, R) abs(x);

% KL divergence is in [0, +\infty), where 0 means equal to random
% assessor. Map it in the (0, 1] range by negative exponential so that 0  
% means different from random assessor
EXPERIMENT.command.normalize.kld = @(x, T, R) exp(-x);

% Commands to be executed to compute weights

EXPERIMENT.command.weight.sgl.msr.md = @(und, uni, ovr, T) repmat(min([und; uni; ovr]), T, 1);

EXPERIMENT.command.weight.sgl.msr.msd = @(und, uni, ovr, T) repmat(min([und.^2; uni.^2; ovr.^2]), T, 1);

EXPERIMENT.command.weight.sgl.msr.med = @(und, uni, ovr, T) repmat(sum([und; uni; ovr]), T, 1);


EXPERIMENT.command.weight.sgl.gp.md = @(und, uni, ovr, T) repmat(min([mean(und);mean(uni);mean(ovr)]), T, 1);

EXPERIMENT.command.weight.sgl.gp.msd = @(und, uni, ovr, T) repmat(min([mean(und).^2;mean(uni).^2;mean(ovr).^2]), T, 1);

EXPERIMENT.command.weight.sgl.gp.med = @(und, uni, ovr, T) repmat(sum([mean(und);mean(uni);mean(ovr)]), T, 1);


EXPERIMENT.command.weight.sgl.wgt.md = @(und, uni, ovr, T) repmat(mean(min(cat(3, und, uni, ovr), [], 3)), T, 1);

EXPERIMENT.command.weight.sgl.wgt.msd = @(und, uni, ovr, T) repmat(mean(min(cat(3, und.^2, uni.^2, ovr.^2), [], 3)), T, 1);

EXPERIMENT.command.weight.sgl.wgt.med = @(und, uni, ovr, T) repmat(mean(sum(cat(3, und, uni, ovr), 3)), T, 1);


EXPERIMENT.command.weight.tpc.msr.md = @(und, uni, ovr, T) min(cat(3, und, uni, ovr), [], 3);

EXPERIMENT.command.weight.tpc.msr.msd = @(und, uni, ovr, T) min(cat(3, und.^2, uni.^2, ovr.^2), [], 3);

EXPERIMENT.command.weight.tpc.msr.med = @(und, uni, ovr, T) sum(cat(3, und, uni, ovr), 3);


EXPERIMENT.command.weight.tpc.gp.md = @(und, uni, ovr, T) min(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), [], 3);

EXPERIMENT.command.weight.tpc.gp.msd = @(und, uni, ovr, T) min(cat(3, mean(und, 3).^2, mean(uni, 3).^2, mean(ovr, 3).^2), [], 3);

EXPERIMENT.command.weight.tpc.gp.med = @(und, uni, ovr, T) sum(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), 3);


EXPERIMENT.command.weight.tpc.wgt.md = @(und, uni, ovr, T) mean(min(cat(4, und, uni, ovr), [], 4), 3);

EXPERIMENT.command.weight.tpc.wgt.msd = @(und, uni, ovr, T) mean(min(cat(4, und.^2, uni.^2, ovr.^2), [], 4), 3);

EXPERIMENT.command.weight.tpc.wgt.med = @(und, uni, ovr, T) mean(sum(cat(4, und, uni, ovr), 4), 3);


%<<<<<<<<<<

EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.id = 'aw_us_mono_sgl_gp_tau_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.latex.label = '\texttt{sgl\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.matlab.label = 'sgl_tau_md';





%>>>>>>>>>>>> aw_us_mono_sgl

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.id = 'aw_us_mono_sgl_gp_fro_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.oldID = 'awusmonofromed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.latex.label = '\texttt{sgl\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.matlab.label = 'sgl_fro_med';

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.id = 'aw_us_mono_sgl_gp_fro_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.oldID = 'awusmonofromd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.latex.label = '\texttt{sgl\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.matlab.label = 'sgl_fro_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.id = 'aw_us_mono_sgl_gp_fro_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.latex.label = '\texttt{sgl\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.matlab.label = 'sgl_fro_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.id = 'aw_us_mono_sgl_gp_rmse_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.oldID = 'awusmonormsemed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.latex.label = '\texttt{sgl\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.matlab.label = 'sgl_rmse_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.id = 'aw_us_mono_sgl_gp_rmse_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.oldID = 'awusmonormsemd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.latex.label = '\texttt{sgl\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.matlab.label = 'sgl_rmse_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.id = 'aw_us_mono_sgl_gp_rmse_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.latex.label = '\texttt{sgl\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.matlab.label = 'sgl_rmse_md';

EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.id = 'aw_us_mono_sgl_gp_kld_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.oldID = 'awusmonokldmed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.latex.label = '\texttt{sgl\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.matlab.label = 'sgl_kld_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.id = 'aw_us_mono_sgl_gp_kld_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.oldID = 'awusmonokldmd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.latex.label = '\texttt{sgl\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.matlab.label = 'sgl_kld_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.id = 'aw_us_mono_sgl_gp_kld_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.latex.label = '\texttt{sgl\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.matlab.label = 'sgl_kld_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.id = 'aw_us_mono_sgl_gp_apc_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.oldID = 'awusmonoapcmed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.latex.label = '\texttt{sgl\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.matlab.label = 'sgl_apc_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.id = 'aw_us_mono_sgl_gp_apc_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.oldID = 'awusmonoapcmd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.latex.label = '\texttt{sgl\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.matlab.label = 'sgl_apc_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.id = 'aw_us_mono_sgl_gp_apc_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.latex.label = '\texttt{sgl\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.matlab.label = 'sgl_apc_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.id = 'aw_us_mono_sgl_gp_tau_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.oldID = 'awusmonotaumed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.latex.label = '\texttt{sgl\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.matlab.label = 'sgl_tau_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.id = 'aw_us_mono_sgl_gp_tau_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.oldID = 'awusmonotaumd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.latex.label = '\texttt{sgl\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.matlab.label = 'sgl_tau_msd';



%<<<<<<<<<<<< aw_us_mono_sgl

%>>>>>>>>>>>> aw_us_mono_tpc

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.id = 'aw_us_mono_tpc_gp_fro_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.oldID = 'awusmonofromed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.latex.label = '\texttt{tpc\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.matlab.label = 'tpc_fro_med';

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.id = 'aw_us_mono_tpc_gp_fro_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.oldID = 'awusmonofromd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.latex.label = '\texttt{tpc\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.matlab.label = 'tpc_fro_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.id = 'aw_us_mono_tpc_gp_fro_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.latex.label = '\texttt{tpc\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.matlab.label = 'tpc_fro_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.id = 'aw_us_mono_tpc_gp_rmse_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.oldID = 'awusmonormsemed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.latex.label = '\texttt{tpc\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.matlab.label = 'tpc_rmse_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.id = 'aw_us_mono_tpc_gp_rmse_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.oldID = 'awusmonormsemd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.latex.label = '\texttt{tpc\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.matlab.label = 'tpc_rmse_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.id = 'aw_us_mono_tpc_gp_rmse_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.latex.label = '\texttt{tpc\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.matlab.label = 'tpc_rmse_md';

EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.id = 'aw_us_mono_tpc_gp_kld_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.oldID = 'awusmonokldmed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.latex.label = '\texttt{tpc\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.matlab.label = 'tpc_kld_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.id = 'aw_us_mono_tpc_gp_kld_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.oldID = 'awusmonokldmd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.latex.label = '\texttt{tpc\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.matlab.label = 'tpc_kld_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.id = 'aw_us_mono_tpc_gp_kld_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.latex.label = '\texttt{tpc\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.matlab.label = 'tpc_kld_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.id = 'aw_us_mono_tpc_gp_apc_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.oldID = 'awusmonoapcmed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.latex.label = '\texttt{tpc\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.matlab.label = 'tpc_apc_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.id = 'aw_us_mono_tpc_gp_apc_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.oldID = 'awusmonoapcmd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.latex.label = '\texttt{tpc\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.matlab.label = 'tpc_apc_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.id = 'aw_us_mono_tpc_gp_apc_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.latex.label = '\texttt{tpc\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.matlab.label = 'tpc_apc_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.id = 'aw_us_mono_tpc_gp_tau_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.oldID = 'awusmonotaumed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.latex.label = '\texttt{tpc\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.matlab.label = 'tpc_tau_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.id = 'aw_us_mono_tpc_gp_tau_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.oldID = 'awusmonotaumd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.latex.label = '\texttt{tpc\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.matlab.label = 'tpc_tau_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.id = 'aw_us_mono_tpc_gp_tau_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.latex.label = '\texttt{tpc\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.matlab.label = 'tpc_tau_md';



%% Patterns for LOG files

% Pattern EXPERIMENT.path.base/log/import_collection_<trackID>.log
EXPERIMENT.pattern.logFile.importCollection = @(trackID) sprintf('%1$simport_collection_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<tag>_measures_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasures = @(trackID, tag) sprintf('%1$scompute_%2$s_measures_%3$s.log', EXPERIMENT.path.log, tag, trackID);

% Pattern EXPERIMENT.path.base/log/generate_kuples_<trackID>.log
EXPERIMENT.pattern.logFile.generateKuples = @(trackID) sprintf('%1$sgenerate_kuples_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_poold_<trackID>.log
EXPERIMENT.pattern.logFile.computePools = @(experimentTag, trackID) sprintf('%1$scompute_%2$s_pools_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_pool_measures_slice-<startKupleSet>-<endKupleSet>-<startKuple>-<endKuple>_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasures = @(experimentTag, startKupleSet, endKupleSet, startKuple, endKuple, trackID) ...
    sprintf('%1$scompute_%2$s_pool_measures_slice-%3$d-%4$d-%5$d-%6$d_%7$s.log', EXPERIMENT.path.log, experimentTag, ...
            startKupleSet, endKupleSet, startKuple, endKuple, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<gap>_<experimentTag>_pool_measures_gap_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasuresGap = @(gap, experimentTag, trackID) ...
    sprintf('%1$scompute_%2$s_%3$s_pool_measures_gap_%4$s.log', EXPERIMENT.path.log, gap, experimentTag, trackID);
        
% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_measures_pdf_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasuresPdf = @(experimentTag, trackID) ...
    sprintf('%1$scompute_%2$s_measures_pdf_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID);

% Pattern EXPERIMENT.path.base/log/analyse_<tag>_measures_slice-<startKupleSet>-<endKupleSet>_<trackID>.log
EXPERIMENT.pattern.logFile.analyseMeasures = @(trackID, tag, startKupleSet, endKupleSet) sprintf('%1$sanalyse_%2$s_measures_slice-%3$d-%4$d_%5$s.log', EXPERIMENT.path.log, tag, startKupleSet, endKupleSet, trackID);

% Pattern EXPERIMENT.path.base/log/print_report_<desc>_<trackID>.log
EXPERIMENT.pattern.logFile.printReport = @(trackID, desc) sprintf('%1$sprint_report_%2$s_%3$s.log', EXPERIMENT.path.log, desc, trackID);


%% Patterns for MAT files

% Pattern EXPERIMENT.path.base/dataset/<trackID>/EXPERIMENT.tag.base.id/dataset_<trackShortID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$sdataset_%5$s.mat', EXPERIMENT.path.base, filesep, trackID, EXPERIMENT.tag.base.id, trackShortID);

% Pattern EXPERIMENT.path.base/dataset/<trackID>/kuples_<trackShortID>.mat
EXPERIMENT.pattern.file.kuples = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$skuples_%4$s.mat', EXPERIMENT.path.base, filesep, trackID, trackShortID);

% Pattern EXPERIMENT.path.base/dataset/<trackID>/<experimentTag>/<poolID>
EXPERIMENT.pattern.file.pool = @(trackID, experimentTag, poolID) ...
    sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, poolID);

% Pattern EXPERIMENT.path.base/measure/<trackID>/<relativePath>/<experimentTag>/<measureID>
EXPERIMENT.pattern.file.measure = @(trackID, relativePath, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID);


EXPERIMENT.pattern.file.measureK15 = @(trackID, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s.k15%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, measureID);

EXPERIMENT.pattern.file.measureK29 = @(trackID, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s.k29%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, measureID);

% Pattern EXPERIMENT.path.base/analysis/<trackID>/<relativePath>/<experimentTag>/<measureID>
EXPERIMENT.pattern.file.analysis = @(trackID, relativePath, experimentTag, measureID) ...
    sprintf('%1$sanalysis%2$s%3$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID);

% Pattern EXPERIMENT.path.base/pre-compute/<granularity>/<aggregation>/<pm>/<trackID>/<experimentTag>/<preID>
EXPERIMENT.pattern.file.pre = @(granularity, aggregation, pm, trackID, experimentTag, preID) ...
    sprintf('%1$spre-compute%2$s%3$s%2$s%4$s%2$s%5$s%2$s%6$s%2$s%7$s%2$s%8$s.mat', ...
    EXPERIMENT.path.base, filesep, granularity, aggregation, pm, trackID, experimentTag, preID);

% Pattern EXPERIMENT.path.base/report/<trackID>/<type>_report_<trackID>.mat
EXPERIMENT.pattern.file.report = @(type, trackID) sprintf('%1$sreport%2$s%4$s%2$s%3$s_report_%4$s.tex', EXPERIMENT.path.base, filesep, type, trackID);

% Pattern EXPERIMENT.path.base/figure/<trackID>/<relativePath>/<anovaID>
EXPERIMENT.pattern.file.anova = @(trackID, relativePath, figureID) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, figureID);

% Pattern EXPERIMENT.path.base/figure/<trackID>/<relativePath>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, relativePath, figureID) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%5$s.pdf', EXPERIMENT.path.base, filesep, trackID, relativePath, figureID);


%% Patterns for identifiers

% Pattern pool_<experimentTag>_<poolLabel>_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(experimentTag, poolLabel, trackID) sprintf('pool_%1$s_%2$s_%3$s', experimentTag, poolLabel, trackID);

% Pattern pool_base_GOLD_<trackID>
EXPERIMENT.pattern.identifier.goldPool = @(trackID) EXPERIMENT.pattern.identifier.pool(EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID);

% Pattern runSet_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.runSet = @(trackID, originalTrackID) sprintf('runSet_p%1$sr%2$s', trackID, originalTrackID);

% Pattern k<size>
EXPERIMENT.pattern.identifier.kuple = @(k) sprintf('k%1$02d', EXPERIMENT.kuples.sizes(k));
EXPERIMENT.pattern.identifier.oldKuple = @(k) sprintf('k%1$02d', k);

% Pattern kuples_k<size>_<trackID>
EXPERIMENT.pattern.identifier.kuples = @(k, trackID) sprintf('kuples_k%1$02d_%2$s', k, trackID);

% Pattern k<size>_s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledKuple = @(k, s) sprintf('k%1$02d_s%2$04d', k, s);

% Pattern s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledRndPool = @(s) sprintf('s%1$04d', s);

% Pattern <measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.measure = @(measureID, experimentTag, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s_%3$s_p%4$sr%5$s', lower(measureID), experimentTag, experimentLabel, trackID, originalTrackID);

% Pattern <pm>_<measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.pm = @(pm, measureID, experimentTag, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s', pm, EXPERIMENT.pattern.identifier.measure(measureID, experimentTag, experimentLabel, trackID, originalTrackID));


% Pattern accuracy_<experimentTag>_<experimentLabel>_<trackID>
EXPERIMENT.pattern.identifier.accuracy =  @(experimentTag, experimentLabel, trackID) ...
    sprintf('accuracy_%1$s_%2$s_%3$s', experimentTag, experimentLabel, trackID);

% Pattern multcompare_<metric>_<measure>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.multcompare.figure = @(metric, measure, shortTrackID, originalShortTrackID) ...
    sprintf('multcompare_%1$s_%2$s_p%3$sr%4$s', lower(metric), lower(measure), shortTrackID, originalShortTrackID);


% Pattern <factors>_<shortTrackID>
EXPERIMENT.pattern.identifier.anova.id = @(factors, shortTrackID) ...
    sprintf('%1$s_%2$s', factors, shortTrackID);

% Pattern <factors>_<metric>_<effects>_<shortTrackID>
EXPERIMENT.pattern.identifier.anova.figure = @(factors, metric, effects, shortTrackID) ...
    sprintf('%1$s_%2$s_%3$s_%4$s', factors, lower(metric), effects, shortTrackID);


% Pattern anovaTable_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.table = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaTable_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern anovaStats_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.stats = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaStats_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern soa_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.soa = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('soa_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);



%% Commands to be executed

% Utility commands
EXPERIMENT.command.util.varList2Cell = @(varargin)(deal(varargin));
EXPERIMENT.command.util.assignVars =  @(varargin)(varargin{:});

% Commands to be executed to import TREC_21_2012_Crowd
EXPERIMENT.command.TREC_21_2012_Crowd.importGoldPool = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'Delimiter', 'space',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importCrowdPool = @(fileName, id, requiredTopics) importCrowd2012Pool('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'RequiredTopics', requiredTopics, 'Delimiter', 'tab',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importRunSet = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', 'TrecEvalLexDesc', 'SinglePrecision', true, 'Verbose', false);

% Commands to be executed to compute the measures
EXPERIMENT.command.measure.ap = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);
EXPERIMENT.command.measure.rbp = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', 0.80, 'FixNumberRetrievedDocuments', 1000);
EXPERIMENT.command.measure.dcg = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix,  'CutOffs', 1000, 'LogBase', 10, 'Normalize', false, 'FixNumberRetrievedDocuments', 1000, 'FixedNumberRetrievedDocumentsPaddingStrategy', 'NotRelevant', 'MapToRelevanceWeights', [0 1]);
EXPERIMENT.command.measure.erap = @(pool, runSet, shortNameSuffix) expectedRandomAveragePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);
EXPERIMENT.command.measure.errbp =  @(pool, runSet, shortNameSuffix) expectedRandomRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', 0.80, 'FixNumberRetrievedDocuments', 1000);
EXPERIMENT.command.measure.erdcg =  @(pool, runSet, shortNameSuffix) expectedRandomDiscountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', 1000);

% Commands to be executed to generate kuples
EXPERIMENT.command.kuples.generate = @(poolsIdx, kupleSize) combnk(poolsIdx, kupleSize);
EXPERIMENT.command.kuples.sample = @(kuples, K, samples) kuples(randperm(K, samples), :);

% Commands to be executed to compute MV pools
EXPERIMENT.command.mvPools.compute = @(poolID, varargin) majorityVotePool(poolID, varargin{:});

% Commands to be executed to compute EM pools
EXPERIMENT.command.emPools.compute = @(tag, poolID, varargin) EXPERIMENT.tag.(tag).em(EXPERIMENT.em.tolerance, EXPERIMENT.em.maxIterations, poolID, varargin{:});

% Commands to be executed to compute RND pools
EXPERIMENT.command.rndPools.compute = @(tag, poolID, goldPool) randomPool(poolID, goldPool, EXPERIMENT.tag.(tag).ratio);

% Commands to be executed to compute binomial MV pools
EXPERIMENT.command.binmvPools.compute = @(poolID, varargin) binomialMergePools(poolID, @mvMerger, varargin{:});

% Commands to be executed to compute quantized binomial MV pools
EXPERIMENT.command.qbinmvPools.compute = @(poolID, varargin) binomialMergePools(poolID, @qmvMerger, varargin{:});

% Commands to be executed to compute sigmoid quantized binomial MV pools
EXPERIMENT.command.s10qbinmvPools.compute = @(poolID, varargin) binomialMergePools(poolID, @s10qmvMerger, varargin{:});

% Commands to be executed to compute sigmoid quantized binomial MV pools
EXPERIMENT.command.sqbinmvPools.compute = @(poolID, varargin) binomialMergePools(poolID, @sqmvMerger, varargin{:});

% Commands to be executed to compute sigmoid quantized binomial MV pools
EXPERIMENT.command.s20qbinmvPools.compute = @(poolID, varargin) binomialMergePools(poolID, @s20qmvMerger, varargin{:});



% Commands to be executed to compute pool measures
EXPERIMENT.command.computePoolMeasures.kupleSize = @(kuples) size(kuples, 1);

% Commands to be executed to compute gaps from random pools
EXPERIMENT.command.rndPools.gap.tpc.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.tpc.apc = @(random, assessor) apCorr(random(:), assessor(:), 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.tpc.tau = @(random, assessor) corr(random(:), assessor(:), 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.tpc.rmse = @(random, assessor) rmseCoeff(random(:), assessor(:));
EXPERIMENT.command.rndPools.gap.tpc.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.tpc.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});


EXPERIMENT.command.rndPools.gap.sgl.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.sgl.apc = @(random, assessor) apCorr(nanmean(random).', nanmean(assessor).', 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.sgl.tau = @(random, assessor) corr(nanmean(random).', nanmean(assessor).', 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.sgl.rmse = @(random, assessor) rmseCoeff(nanmean(random).', nanmean(assessor).');
EXPERIMENT.command.rndPools.gap.sgl.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.sgl.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});

% Commands to be executed to compute AWARE measures
EXPERIMENT.command.aware = @(a, kuples, measures) aware(a, kuples, measures{:});

% Commands to be executed to analyse AWARE measures
EXPERIMENT.command.analyse = @(goldMeasure, awareMeasure) analyseKuples(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure);


