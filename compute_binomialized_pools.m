%% compute_pool_measures
% 
% Compute measures based on learned pools, e.g. majority vote, and 
% saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_pool_measures(trackID, tag, startKupleSet, endKupleSet, startKuple, endKuple)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|startKupleSet|* - the index of the start kuples set. Optional.
% * *|endKupleSet|* - the index of the end kuples set. Optional.
% * *|startKuple|* - the index of the start kuple within a set. Optional.
% * *|endKuple|* - the index of the end kuple within a set. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_binomialized_pools(trackID, tag, p_notrel, p_rel)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that tag is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    

    % setup common parameters
    common_parameters;
    
    startKupleSet = 1;
    endKupleSet = EXPERIMENT.kuples.number;
    startKuple = 1;
    endKuple = EXPERIMENT.kuples.samples;
    

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s binomialized pools on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start k-uple set %d\n', startKupleSet);
    fprintf('    * end k-uple set %d\n', endKupleSet);
    fprintf('    * start k-uple %d\n', startKuple);
    fprintf('    * end k-uple %d\n', endKuple);

    start = tic;
    fprintf('  - loading data sets \n');
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
        
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));
        
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    % for each k-uple set
    for k = startKupleSet:endKupleSet
        
        evalf(EXPERIMENT.command.computePoolMeasures.kupleSize, ...
            {kuplesIdentifiers{k}}, ...
            {'KK'});
        
        % for each k-uple
        for kk = min(startKuple, KK):min(endKuple, KK)
            
            startPool = tic;
            
            poolID = EXPERIMENT.pattern.identifier.pool(tag, ...
                EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), ...
                EXPERIMENT.(trackID).shortID);
            
            bizPoolID = EXPERIMENT.pattern.identifier.pool(['biz' tag], ...
                EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), ...
                EXPERIMENT.(trackID).shortID);
            
            fprintf('\n+ Pool: %s\n', poolID);
            
            % load the pool
            serload2(EXPERIMENT.pattern.file.pool(trackID, tag, poolID), ...
                'WorkspaceVarNames', {'pool'}, ...
                'FileVarNames', {poolID});
            
            
            bizPool = pool;
            
            for t = 1:height(pool)
                
                topic = pool{t, 1}{1, 1};
                
                notRel = topic{:, 2} == 'NotRelevant';
                
                topic(:, 2) = [];
                topic{:, 2} = p_rel;
                topic{notRel, 2} = p_notrel;
                topic.Properties.VariableNames = {'Document', 'RelevanceDegree'};
                
                bizPool{t, 1}{1, 1} = topic;
                
            end;
            
            bizPool.Properties.UserData.identifier = bizPoolID;
            bizPool.Properties.UserData.fileName = bizPoolID;
            
            % save the pool
            sersave2(EXPERIMENT.pattern.file.pool(trackID, ['biz' tag], bizPoolID), ...
                'WorkspaceVarNames', {'bizPool'}, ...
                'FileVarNames', {bizPoolID});
            
            fprintf('  - elapsed time for pool %s: %s\n', poolID, elapsedToHourMinutesSeconds(toc(startPool)));
            
            clear poolID bizPoolID pool bizPool;
            
            
        end; % for kuple
        
        fprintf('     * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    end; % for kupleSet

    fprintf('\n\n######## Total elapsed time for computing %s binomialized pools on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
        
    diary off;
end
