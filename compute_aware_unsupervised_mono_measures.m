%% compute_aware_unsupervised_mono_measures
% 
% Computes and saves AWARE mono-feature unsupervised measures.

%% Synopsis
%
%   [] = compute_aware_mono_unsupervised_measures(trackID, tag)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * Ferrante, M., Ferro, N., and Maistro, M. (2015). UNIPD Internal Report.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_aware_unsupervised_mono_measures(trackID, tag, mid)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');

     if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
     % check that mid is a non-empty string
    validateattributes(mid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');

     if iscell(mid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(mid) && numel(mid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    mid = char(strtrim(mid));
    mid = mid(:).';
    
    % setup common parameters
    common_parameters;


    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing %s aware unsupervised mono-feature measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - scoring scheme %s\n', tag);

    % local version of the common parameters
    DATA.EXPERIMENT = EXPERIMENT;
    DATA.tag = tag;
    DATA.relativePath = EXPERIMENT.tag.(tag).relativePath;
    DATA.trackID = trackID;
    DATA.shortTrackID = EXPERIMENT.(trackID).shortID;
        
    % Load the number of pools and topics
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'P', 'T', 'poolLabels');

    DATA.T = T;
    DATA.P = P;
    DATA.poolLabels = poolLabels;
    
    clear('P', 'T', 'poolLabels');
     
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));

    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        
        DATA.originalTrackID = EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r};
        
        fprintf('\n+ Original track of the runs: %s\n', DATA.originalTrackID);
        
        DATA.originalTrackShortID = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        start = tic;
        
        fprintf('  - computing %s %s\n', tag, mid);
        
        fprintf('    * loading base measures\n');
        
        measures = cell(1, DATA.P);
        for p = 1:DATA.P
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.tag.base.id, DATA.poolLabels{p}, ...
                EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
            
            serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID), measureID);
            
            eval(sprintf('measures{%1$d} = %2$s;', p, measureID));
            
            clear(measureID);
        end;
        clear measureID
        
        % number of runs
        DATA.R = width(measures{1});
        DATA.topics = measures{1}.Properties.RowNames;
        
        fprintf('    * computing assessor scores\n');
        
        DATA.weightID = EXPERIMENT.pattern.identifier.pm('weight', mid, tag, '', ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
        
        DATA.measure = mid;
        A = EXPERIMENT.tag.(tag).score.command(DATA);
        
        assert(size(A, 1) == DATA.T, 'The assessor weight matrix has %s rows instead of %d.', size(A, 1), DATA.T);
        assert(size(A, 2) == DATA.P, 'The assessor weight matrix has %s columns instead of %d.', size(A, 2), DATA.P);
        
        fprintf('    * processing measures\n');
        
        % for each k-uple
        for k = 1:EXPERIMENT.kuples.number
            
            fprintf('      # k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
            
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
            
            
            evalf(EXPERIMENT.command.aware, ...
                {'A', kuplesIdentifiers{k}, 'measures'}, ...
                {measureID});
            
            sersave(EXPERIMENT.pattern.file.measure(trackID, DATA.relativePath, tag, measureID), measureID(:));
            
            clear(measureID);
        end; % k-uple size
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
        
    end % for runset


    fprintf('\n\n######## Total elapsed time for computing %s aware mono-feature unsupervised measures on collection %s (%s): %s ########\n\n', ...
            tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));    
    
    diary off;     
    

end
