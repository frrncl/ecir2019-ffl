%% assess
% 
% Assesses the given run(s) with respect to the given pool.

%% Synopsis
%
%   [assessedRunSet, poolStats, runSetStats, inputParams] = assess(pool, runSet, varargin)
%  
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html 
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html 
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html 
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|NotAssessed|* (optional) - a string indicating how not assessed
% documents, i.e. those in the run but not in the pool, have to be
% processed: |NotRevelant|, the minimum of the relevance degrees of the 
% pool is used as |NotRelevant|; |Undefined|, the categorical
% <http://www.mathworks.it/it/help/matlab/ref/isundefined.html undefined> 
% is used as |NotAssessed|; |Condensed|, the not assessed documents are 
% removed from the run. If not specified, the default value  is 
% |NotRelevant| to mimic the behaviour of trec_eval.
% * *|MapToBinaryRelevance|* (optional) - a string specifying whether and
% how relevance degrees have to be mapped to binary relevance. The
% following values can be used: _|Hard|_ considers only the maximum degree
% of relevance in the pool as |Relevant| and any degree below it as
% |NotRelevant|; _|Lenient|_ considers any degree of relevance in the pool
% above the minimum one as |Relevant| and only the minimum one is
% considered as |NotRelevant|; _|RelevanceDegree|_ considers the relevance
% degrees in the pool stricly above the specified one as |Relevant| and all
% those less than or equal to it as |NotRelevant|. In this latter case, if
% |RelevanceDegree| does not correspond to any of the relevance degrees in
% the pool, an error is raised. If not specified, mapping to binary
% relevance will not be performed.
% * *|MapToRelevanceWeights|* (optional) - a vector of numbers to which the
% relevance degrees in the pool will be mapped. It must be an increasing
% vector with as many elements as the relevance degrees in the pool are. In
% case also |MapToBinaryRelevance| is used, it must have two elements. If 
% not specified, mapping to relevance weights will not be performed.
% * *|FixNumberRetrievedDocuments|* (optional) - an integer value
% specifying the expected length of the retrieved document list. Topics
% retrieving more documents than |FixNumberRetrievedDocuments| will be 
% truncated at |FixNumberRetrievedDocuments|; tocpis retrieving less
% documents than |FixNumberRetrievedDocuments| will be padded with
% additional documents according to the strategy defined in 
% |FixedNumberRetrievedDocumentsPaddingStrategy|. If not specified, the 
% value 1,000 will be used as default, to mimic the behaviour of trec_eval.
% Pass an empty matrix to let topics retrieve a variable number of
% documents.
% * *|FixedNumberRetrievedDocumentsPaddingStrategy|* (optional) - a string
% specifying how topics with less than |FixNumberRetrievedDocuments| have
% to be padded. The following values can be used: _|NotRelevant|_ documents
% assessed as |NotRelevant|, i.e. the minimum relevance degree in the pool,
% are added; _|NotAssessed|_ documents not assessed are added, the 
% categorical <http://www.mathworks.it/it/help/matlab/ref/isundefined.html
% undefined> is used as |NotAssessed|; _|NotRelevantAfterRecallBase|
% documents |NotAssessed| are added up to the recall base for that topic 
% and, after it, |NotRelevant| ones. If not specified, |NotRelevant| is 
% used as default to mimic the behaviour of trec_eval.
% * *|RemoveUUFromPool|* (optional) - a boolean indicating whether
% unsampled or unjudged documents have to be removed from the pool before
% assessing the run. The default is true.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is 
% used as default. 
%
% *Returns*
%
% * *|assessedRunSet|*  - a table containing a row for each topic and a column 
% for each run named |runName|. The value contained in the column, for each 
% row, is a cell array of one element, wrapping a table with one column:
% |Assessment| with the assessments for the retrieved documents for
% that topic. The |UserData| property of  the table contains a struct 
% with the following fields: _|identifier|_ is the identifier of the run. 
% * *|poolStats|* - a table containing statistics about the pool. Rows are 
% topics while the columns are: _|RelevanceDegrees|_, one colum for each
% relevance degree in the pool reporting the total number of documents for
% that relevance degree; _|RecallBase|_ reporting the recall base, i.e. the
% total number of documents strictly above the minimum relevance degree in
% the pool; _|Assessment|_ reporting the total number of assessed documents
% for that topic; _|BinaryNotRelevant|_ reporting the number of
% |NotRelevant| documents when mapping to binary relevance; 
% _|BinaryRelevant|_ reporting the number of |Relevant| document when 
%  mapping to binary relevance.
% * *|runSetStats|* - a table containing statistics about the run set where
% each row is a topic and columns are runs. Each table cell contains a
% strcut with the following fields: a field for each _|RelevanceDegree|_
% reporting the total number of documents for that relevance degree in that
% topic; _|notAssessed|_ reporting the total number of not assessed
% documents for that topic; _|relevantRetrieved|_ reporting the total
% number of relevant retrieved documents for that topic, i.e. the
% total number of documents strictly above the minimum relevance degree; 
%  _|retrieved|_ reporting the total number of retrieved 
% documents for that topic. The structure contains a substructure
% _|original|_ with the same information as above but before any processing
% is applied, e.g. mapping to binary relevance, mapping to relevance weight
% or padding. The structure contains another substructure _|binary|_ with
% two fields: the minimum relevance degree in the pool reporting the total
% number of not relevant documents for that topic and the maximum relevance
% degree in the pool reporting the total number of relevant documents for
% that topic. The structure contains another substructure
% _|fixNumberRetrievedDocuments|_ with two fields: _|paddedDocuments|_ the
% total number of documents added for that topic, if any, and 
% _|discardedDocuments|_ the total number of documents discarder for that
% topic, if any.
% * *|inputParams|* - a struct summarizing the input parameters passed.

%% Example of use
%  
%   assessedRun = assess(pool, run);
%
% In this example each run has two topics, |351| and |352|. It returns the 
% following table.
%
%              APL985LC          APL985SC          AntHoc01   
%           ______________    ______________    ______________
%
%    351    [1000x1 table]    [1000x1 table]    [1000x1 table]
%    352    [1000x1 table]    [1000x1 table]    [1000x1 table]
%
% Column names are run identifiers, row names are topic identifiers.
% 
%   APL985LC_351 = assessedRun{'351','APL985LC'}{1, 1};
%
% It returns the table containing the assessed documents for topic
% |351| and run |APL985LC|.
%
%   APL985LC_351(1:5,1)
% 
% It returns the first five documents assessed for run |APL985LC| for topic
% |351|.
%
%    Assessment
%    _______________
%
%     not_relevant   
%     not_relevant   
%     not_relevant   
%     not_relevant   
%     not_relevant   
%% References
% 
% For condensed result lists (|Condensed| in parameter |NotAssessed|), 
% please refer to:
%
% * Sakai, T. (2007). Alternatives to Bpref. In Kraaij, W., de Vries, A. P., 
% Clarke, C. L. A., Fuhr, N., and Kando, N., editors, _Proc. 30th Annual 
% International ACM SIGIR Conference on Research and Development in 
% Information Retrieval (SIGIR 2007)_, pages 71?78. ACM Press, New York, 
% USA.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [assessedRunSet, inputParams] = binomialAssess(pool, runSet, varargin)
    
    % cache of already assessed run sets
    persistent ASSESSMENT_CACHE;
    
    % check that we have the correct number of input arguments. 
    narginchk(2, inf);
    
    % check that pool is a non-empty table
    validateattributes(pool, {'table'}, {'nonempty'}, '', 'pool', 1);
    
    % check that runSet is a non-empty table
    validateattributes(runSet, {'table'}, {'nonempty'}, '', 'runSet', 2);
    
    % check that the pool and the runSet tables refer to the same set of
    % topics
    if ~isequal(pool.Properties.RowNames, runSet.Properties.RowNames)
        poolOnly = setdiff(pool.Properties.RowNames, runSet.Properties.RowNames).';
        runOnly = setdiff(runSet.Properties.RowNames, pool.Properties.RowNames).';
        msg = [];
        
        if ~isempty(poolOnly)
            msg = sprintf('The following topics are only in the pool: %s. ', ...
                strjoin(poolOnly, ', '));
        end
        
        if ~isempty(runOnly)
            msg = sprintf('%sThe following topics are only in the run set %s.', ...
                msg, strjoin(runOnly, ', '));
        end
        
        error('MATTERS:IllegalArgument', 'The pool %s and the run set %s must refer to the same set of topics. %s', ...
            pool.Properties.UserData.identifier, runSet.Properties.UserData.identifier, msg);
    end
        
    % total number of topics to be assessed
    inputParams.topics = length(pool.Properties.RowNames);
  
    % total number of runs to be assessed
    inputParams.runs = length(runSet.Properties.VariableNames);

    % parse the variable inputs
    pnames = {'FixNumberRetrievedDocuments' 'Verbose'};
    dflts =  {1000                          false};
    
    if verLessThan('matlab', '9.2.0')
        [fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end    
    

    % check whether the parameters have been passed by the user (and then 
    % perform additional checks) or they are set to their default  
    
     
    if supplied.FixNumberRetrievedDocuments 
        % check that FixNumberRetrievedDocuments is a scalar integer value 
        % greater than 0
        validateattributes(fixNumberRetrievedDocuments, {'numeric'}, ...
            {'scalar', 'integer', '>', 0}, '', 'FixNumberRetrievedDocuments');
    end
    inputParams.fixNumberRetrievedDocuments = fixNumberRetrievedDocuments;
    
    
    if supplied.Verbose
        % check that verbose is a non-empty scalar
        % logical value
        validateattributes(verbose, {'logical'}, {'nonempty','scalar'}, '', 'Verbose');
    end
    inputParams.verbose = verbose;       
      
    if verbose
        fprintf('\n\n----------\n');
        
        fprintf('Assessing run set %s with respect to pool %s: %d run(s) and %d topic(s) to be assessed.\n\n', ...
            runSet.Properties.UserData.identifier, pool.Properties.UserData.identifier, inputParams.runs, inputParams.topics);

        fprintf('Settings:\n');
        fprintf('  - not assessed documents are: %s;\n', inputParams.notAssessed);
                
        if ~isempty(fixNumberRetrievedDocuments)
            fprintf('  - fixed number of retrieved documents enabled. The threshold for the number of retrieved documents per topic is %d;\n', fixNumberRetrievedDocuments);
            fprintf('    + topics above the threshold will discard documents, topics below the threshold will pad documents;\n');
            fprintf('    + padded documents are assumed to be %s;\n', inputParams.fixedNumberRetrievedDocumentsPaddingStrategy);
        else
            fprintf('  - fixed number of retrieved documents not enabled. Runs may retrieve a different number of documents per topic;\n');
        end
        
        fprintf('\n');
    end
    
    % check whether we have the results in the cache
    if isCached()        
        if verbose
            fprintf('Assessments have been previously computed, returning the cached results.\n');
        end
        
        return;
    end
            
    % the topic currently under assessment
    ct = 1;
        
    % the run currently under assessment
    cr = 1;
            
    % perform the assessment topic-by-topic
    assessedRunSet = rowfun(@assessTopic, runSet, 'OutputVariableNames', runSet.Properties.VariableNames, 'OutputFormat', 'table', 'ExtractCellContents', true, 'SeparateInputs', false);
    assessedRunSet.Properties.UserData.identifier = runSet.Properties.UserData.identifier;

    % cache the assessment for the current input
    cache();
    
    if verbose
        fprintf('\nAssessment cached.\n');
        fprintf('Assessment completed.\n');
    end
    
    %%
   
    % compute the assessments for a given topic over all the runs
    function [varargout] = assessTopic(topic)

        
        if(verbose)
            fprintf('Assessing topic %s (%d out of %d)\n', pool.Properties.RowNames{ct}, ct, inputParams.topics);
            fprintf('  - run(s): ');
        end
        
        % reset the index of the run under assessment for each processed 
        % topic
        cr = 1;
        
        % compute the assessments only on those column which contain the
        % actual runs
        varargout = cellfun(@assessRun, topic);
        
        % increment the index of the current topic under assessment
        ct = ct + 1;    
        
         if(verbose)
            fprintf('\n');
         end
        
        %%
        
        % compute the assessment for a given topic of a given run
        function [judgement] = assessRun(runTopic)
            
            if(verbose)
                fprintf('%s ', runSet.Properties.VariableNames{cr});
            end
                                   
            
            % compute the assessment by joining the documents retrieved by
            % the run with the pool for that topic
            
            % extract the documents to be judged (the first column is
            % Document
            documents = runTopic{:, 1};
            
            % prepare a vector with dummy judgements with the same length
            % of the run
            judgement = zeros(length(documents), 1);
                        
            % find where the documents in the run topic appear in the pool
            % topic (the first column of pool is Document)
            [lia, loc] = ismember(documents, pool{ct, 1}{1, 1}{:, 1});
            
            % copy the relevance judgements from the pool to the
            % corresponding positions in the run
            judgement(lia) = pool{ct, 1}{1, 1}{loc(lia), 2};
            
            % check whether we have to return a fixed number of documents
            % for each topic
            if ~isempty(fixNumberRetrievedDocuments)
                
                h = size(judgement, 1);
                
                 % if there are more retrieved documents than the threshold,
                % simply remove them
                if h > fixNumberRetrievedDocuments
                    judgement = judgement(1:fixNumberRetrievedDocuments, :);
                else
                    judgement = [judgement; zeros(fixNumberRetrievedDocuments - h , 1)];
                end
            end
                    
            % transform it into a table
            judgement = table(judgement, 'VariableNames', {'Assessment'});         
            
            % properly wrap the results into a cell in order to fit it into
            % a value for a table
            judgement = {{judgement}};
            
            % increment the index of the current run under assessment
            cr = cr + 1;
        end
    end


    %%
    
    % generates the field names in the cache for the current input.
    %
    % The structure of the cache is as follows
    % cache.<pool-identifier>.<runSet-identifier>.<notAssessed>.
    %       <mapToBinaryRelevance>.<mapToRelevanceWeights>
    %
    % It uses MD5 hash of the field parameters to ensure fixed length
    % strings as field names and to not exceed the 63 characters MATLAB
    % limit to identifiers
    function [fieldNames] = generateCacheFieldNames()
        
        % allocate the array for the fieldNames
        fieldNames = cell(3, 1);
        
        fieldNames{1} = ['pool_' md5(pool.Properties.UserData.identifier)];
        fieldNames{2} = ['runSet_' md5(runSet.Properties.UserData.identifier)];
        
        if ~isempty(fixNumberRetrievedDocuments)
            fieldNames{3} = ['fixNumberRetrievedDocuments_', ...
                md5(num2str(fixNumberRetrievedDocuments(:).'))];
        else
            fieldNames{3} = 'fixNumberRetrievedDocuments_none';
        end                        
    end

    %%
    
    % checks whether there is a cached assessment for the current input 
    % and, when true, sets the output variables properly
    function [cached] = isCached()
                
        try 
            % get the cached results, if any
            fieldNames = generateCacheFieldNames();
            s = getfield(ASSESSMENT_CACHE, fieldNames{:});
            
            % if there are cached results, sets the output variables
            % accordingly
            assessedRunSet = s.assessedRunSet;
            inputParams = s.inputParams;
        
            % indicate that we are using cached results
            cached = true;
        catch err           
            % when an error is raised, it means that the (sub-)fields does
            % not exist in the cache, and so indicate that there are no
            % cached results
            cached = false;
        end;
    end;
    
    %%
    
    % caches  the assessment for the current input
    function cache()
                         
        % temporary struct for copying data into the cache
        s.assessedRunSet = assessedRunSet;
        s.inputParams = inputParams;
    
        fieldNames = generateCacheFieldNames();
        ASSESSMENT_CACHE = setfield(ASSESSMENT_CACHE, fieldNames{:}, s);
    end
        
end



