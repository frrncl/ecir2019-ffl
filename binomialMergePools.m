%% binomialMergePools
% 
% Computes the generalized recall, as defined by [Kek�l�inen and J�rvelin, 2002]. 
% Generalized recall works only with graded relevance judgments and it is 
% not possible to ask for a mapping to binary relevance.

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = generalizedRecall(pool, runSet, Name, Value)
%  
% It returns NaN if there are no relevant documents in the pool.
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html 
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html 
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html 
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|MapToRelevanceWeights|* (optional) - a vector of numbers to which the
% relevance degrees in the pool will be mapped. It must be an increasing
% vector with as many elements as the relevance degrees in the pool are.
% The default is |[0, 1, 2, ...]| up to as many relevance degrees are in
% the pool.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is 
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column 
% for each run named |runName|. Each cell of the table contains a scalar
% representing the generalized recall. The |UserData| property of  the table 
% contains a struct  with the  following fields: _|identifier|_ is the 
% identifier of the run; _|name|_  is the name of the computed measure, i.e.
% |generalizedRecall|; _|shortName|_ is a short name of the computed 
% measure, i.e. |gR|; _|pool|_ is the identifier of the pool with respect 
% to which the measure has been computed. 
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.
%
%% References
% 
% Please refer to :
%
% * Kek�l�inen, J. and J�rvelin, K. (2002). Using Graded Relevance Assessments
% in IR Evaluation. _Journal of the American Society for Information Science and Technology (JASIST)_, 
% 53(13):1120-1129.
% 
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Author*: <mailto:elosiouk@math.unipd.it Eleonora Losiouk>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2018b or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [mergedPool] = binomialMergePools(mergedPoolID, merger, varargin)
    
    % check that we have the correct number of input arguments.
    narginchk(2, inf);
    
    
    if iscell(mergedPoolID)
        % check that mergedPoolID is a cell array of strings with one element
        assert(iscellstr(mergedPoolID) && numel(mergedPoolID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mergedPoolID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    mergedPoolID = char(strtrim(mergedPoolID));
    mergedPoolID = mergedPoolID(:).';
    
    % check that the mergedPoolID is ok according to the matlab rules
    if ~isempty(regexp(mergedPoolID, '\W*', 'once'))
        error('MATTERS:IllegalArgument', 'mergedPoolID %s is not valid: it can contain only letters, numbers, and the underscore.', ...
            mergedPoolID);
    end
    
    % if it starts with an underscore, remove it since il will be
    % appended afterwards
    if strcmp(mergedPoolID(1), '_')
        mergedPoolID = mergedPoolID(2:end);
    end
            
    % ensure we have a function handle
    validateattributes(merger, {'function_handle'}, {'nonempty'}, '', 'merger', 1);
      
    % assume the first one is a table
    topics = varargin{1}.Properties.RowNames;
    
    % check input tables
    for k = 1:length(varargin)

        % ensure we have a table of measures as input
        validateattributes(varargin{k}, {'table'}, {'nonempty'}, '', 'pool', k);
        
        % check that the pools all refer to the same topics
        if ~isequal(varargin{k}.Properties.RowNames, topics)

            error('MATTERS:IllegalArgument', 'Pool %s refers to topics different from those of pool %s.', ...
            varargin{k}.Properties.UserData.identifier, varargin{1}.Properties.UserData.identifier);       
        end
                
       % ensure that, for each topic, the documents in the pool are ordered
       % the same way
       for t = 1:height(varargin{k})           
            varargin{k}{t, 1}{1, 1} = sortrows(varargin{k}{t, 1}{1, 1}, 1);
       end
    end
    
    % check that all the input tables are about the same documents
    for k = 2:length(varargin)
                               
        % for each topic, check the pools are about the same documents
        for t = 1:height(varargin{k-1})
            
            if ~isequal(varargin{k-1}{t, 1}{1, 1}{:, 1}, ...
                varargin{k}{t, 1}{1, 1}{:, 1})
            
                error('MATTERS:IllegalArgument', 'Pool %s refers to different documents from those of pool %s on topic %s.', ...
                varargin{k-1}.Properties.UserData.identifier, varargin{k}.Properties.UserData.identifier, ...
                varargin{k-1}.Properties.RowNames{t});    
            
            end
            
        end
    end
    
    mergedPool = merger(varargin{:});
    mergedPool.Properties.UserData.identifier = mergedPoolID;    
    mergedPool.Properties.UserData.fileName = mergedPoolID;
end
