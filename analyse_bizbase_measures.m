%% analyse_base_measures
% 
% Analyses the base measures at different k-uples sizes and saves 
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = analyse_base_measures(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_bizbase_measures(trackID, goldtag, goldmid, mid)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that mid is a non-empty string
    validateattributes(mid,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');

    if iscell(mid)
        % check that mid is a cell array of strings with one element
        assert(iscellstr(mid) && numel(mid) == 1, ...
            'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    mid = char(strtrim(mid));
    mid = mid(:).';

    % setup common parameters
    common_parameters;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Analysing %s measures on collection %s (%s) ########\n\n', EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
    fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);

    % load data
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'poolLabels', 'poolIdentifiers', 'T', 'P');

    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number

        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});


        start = tic;

        fprintf('  - analysing %s %s\n', goldtag, goldmid);

        % loading the gold standard measure
        goldMeasureID = EXPERIMENT.pattern.identifier.measure(goldmid, goldtag, EXPERIMENT.label.goldPool, ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        fprintf('    * loading gold standard measure: %s\n', goldMeasureID);

        serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, goldtag, goldMeasureID), goldMeasureID);

        % determine the total number of runs
        evalf(@width, {goldMeasureID}, {'R'})


        % the raw data, each TxR plane is a measure for T topics and R runs
        % to be weighted by assessors' scores and the P planes correspond
        % to the different assessors
        data = NaN(T, R, P);

        fprintf('    * loading assessor measures\n');
        % for each pool
        for p = 1:P

            assessorMeasureID = EXPERIMENT.pattern.identifier.measure(mid, 'bizbase', poolLabels{p}, ...
                EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

            serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, 'bizbase', assessorMeasureID), assessorMeasureID);

            eval(sprintf('data(:, :, p) = %1$s{:, :};', assessorMeasureID));

            clear(assessorMeasureID);
        end;
        clear assessorMeasureID;


        fprintf('    * analysing measures\n');

        measureID = EXPERIMENT.pattern.identifier.measure(mid, goldtag, 'bizbase', ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        tauID = EXPERIMENT.pattern.identifier.pm('tau', mid, goldtag, 'bizbase', ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        apcID = EXPERIMENT.pattern.identifier.pm('apc', mid, goldtag, 'bizbase', ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        rmseID = EXPERIMENT.pattern.identifier.pm('rmse', mid, goldtag, 'bizbase', ...
            EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});

        evalf(EXPERIMENT.command.analyse, ...
            {goldMeasureID, 'data'}, ...
            {rmseID, tauID, apcID});

        sersave(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.base.relativePath, goldtag, measureID), tauID(:), apcID(:), rmseID(:));

        clear(goldMeasureID, tauID, apcID, rmseID);

        clear data;

        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));


    end; % for each run set


    fprintf('\n\n######## Total elapsed time for analysing %s measures on collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

end
